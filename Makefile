# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along
# with this software. If not, see
# <https://creativecommons.org/publicdomain/zero/1.0/>.


.PHONY: all clean


all: shanghai-ccp-member.db shanghai-ccp-member.csv preview/shanghai-ccp-member.csv


shanghai-ccp-member.db: tmp/shanghai-ccp-member-1.csv tmp/shanghai-ccp-member-2.csv
	cat tmp/shanghai-ccp-member-1.csv tmp/shanghai-ccp-member-2.csv | script/csv-to-sqlite shanghai-ccp-member.db

shanghai-ccp-member.csv: shanghai-ccp-member.db
	sqlite3 -readonly -header -csv shanghai-ccp-member.db "pragma foreign_keys = on; select * from member;" >shanghai-ccp-member.csv

preview/shanghai-ccp-member.csv: shanghai-ccp-member.csv
	mkdir -p preview
	head -n 8192 shanghai-ccp-member.csv >preview/shanghai-ccp-member.csv


tmp/shanghai-ccp-member-1.csv: untrusted-blob-of-raw-data/上海市黃俄黨黨員信息集合第一部分.xlsx
	mkdir -p tmp
	ssconvert untrusted-blob-of-raw-data/上海市黃俄黨黨員信息集合第一部分.xlsx tmp/shanghai-ccp-member-1.csv

tmp/shanghai-ccp-member-2.csv: untrusted-blob-of-raw-data/上海市黃俄黨黨員信息集合第二部分.xlsx
	mkdir -p tmp
	ssconvert untrusted-blob-of-raw-data/上海市黃俄黨黨員信息集合第二部分.xlsx tmp/shanghai-ccp-member-2.csv


clean:
	rm -rf tmp preview shanghai-ccp-member.db shanghai-ccp-member.csv
