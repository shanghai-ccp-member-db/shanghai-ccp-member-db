上海市黃俄黨黨員信息集合 / Shanghai CCP member DB
=============================================

- [預覽 / Online preview](https://gitlab.com/shanghai-ccp-member-db/shanghai-ccp-member-db/-/blob/master/preview/shanghai-ccp-member.csv?expanded=true&viewer=simple) (8191 / 1956731 rows)
- [下載 / Download](https://gitlab.com/shanghai-ccp-member-db/shanghai-ccp-member-db/-/raw/master/shanghai-ccp-member.csv) full csv file
- [下載 / Download](https://gitlab.com/shanghai-ccp-member-db/shanghai-ccp-member-db/-/raw/master/shanghai-ccp-member.db) full sqlite database

```
SHA384 (shanghai-ccp-member.csv) = ddf0943907b202fb8a5e9770e2d755fc752a62a1831fc0a9c1172155b1804fbfee19cdf5b4b31c7e37ec34baccc362c7
SHA384 (shanghai-ccp-member.db) = 612300fc76e1a196e1546d29b6ef2fe315d00560c2e168619486c0b4e7f5c7785c18b84f150712351909bae048831b61
```

Untrusted blob of raw data
--------------------------
Open at your own risk!

- [Part 1](https://gitlab.com/shanghai-ccp-member-db/shanghai-ccp-member-db/-/raw/master/untrusted-blob-of-raw-data/%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%83%E4%BF%84%E9%BB%A8%E9%BB%A8%E5%93%A1%E4%BF%A1%E6%81%AF%E9%9B%86%E5%90%88%E7%AC%AC%E4%B8%80%E9%83%A8%E5%88%86.xlsx)
- [Part 2](https://gitlab.com/shanghai-ccp-member-db/shanghai-ccp-member-db/-/raw/master/untrusted-blob-of-raw-data/%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%BB%83%E4%BF%84%E9%BB%A8%E9%BB%A8%E5%93%A1%E4%BF%A1%E6%81%AF%E9%9B%86%E5%90%88%E7%AC%AC%E4%BA%8C%E9%83%A8%E5%88%86.xlsx)

```
SHA384 (上海市黃俄黨黨員信息集合第一部分.xlsx) = cb4108956d74f46ef39a5ae0e7039e365e79a1c45d44a0e589fb5b5272b0c8a760a3440f0551f233dba9a07d28589062
SHA384 (上海市黃俄黨黨員信息集合第二部分.xlsx) = 9bf5c1b42177993a0aa05c193ba28e8a08e69b965d8d5f6f04111f5a3f9aa05fca7818d7459a5605515b55de7aca7763
```

Source
------
- https://lihkg.com/thread/2162349/page/1

CSV header
----------
```
id,name,sex,ethnicity,hometown,organization,id_card_num,address,mobile_num,phone_num,education
```

Sqlite schema
-------------
```
CREATE TABLE member(-- 上海市黃俄黨黨員信息集合
                    id integer unique check(typeof(id) = 'integer' and id >= 1),
                    name text check(typeof(name) = 'text'), -- 姓名
                    sex text check(typeof(sex) = 'text'), -- 性别
                    ethnicity text check(typeof(ethnicity) = 'text'), -- 民族
                    hometown text check(typeof(hometown) = 'text'), -- 籍贯
                    organization text check(typeof(organization) = 'text'), -- 所属党组织名称
                    id_card_num text check(typeof(id_card_num) = 'text'), -- 身份证号
                    address text check(typeof(address) = 'text'), -- 家庭地址
                    mobile_num text check(typeof(mobile_num) = 'text'), -- 手机号码
                    phone_num text check(typeof(phone_num) = 'text'), -- 联系电话
                    education text check(typeof(education) = 'text') -- 学历
                    );
```

Dependencies
------------
- gnumeric
- pypy3
- sqlite3

Build instruction
-----------------
```
make
```

Copyright
---------
All code are dedicated to the public domain via CC0, see
<https://creativecommons.org/publicdomain/zero/1.0/>.
All data are found on the web. They are for educational purposes such as for
identifying Shanghai CCP members.
